# Instalación 


Para instalar  Ruby on Rails en equipos windows  necesita un instalador
 [ este lo puede encontrar aqui](http://railsinstaller.org/fr-FR).
 La instalacion es  similar a  todos los software que se han instalado  en su equipo
 a menos de que usted requiera una instalacion personalizada.
 
Para crear un proyecto en windows debe asegurarse de  que tenga instalado 
ruby y rails.
Para realizar el anterior proceso puede  ejecutar en el CMD
    
    =>$ ruby -v
        
    =>ruby 2.3.3p222 (2016-11-21 revision 56859) [i386-mingw32]
    
    =>$ rails -v 
    
    => Rails 5.1.6


 Para  crear  un proyecto y correrlo debe ejecutar 
     => $ rails new name_proyect     ----------->Instalación
       
     => $ rails server   --------------------->Ejecucion de la aplicación
       