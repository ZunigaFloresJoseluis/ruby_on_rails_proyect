# Ruby on Rails

## Framework 
### ¿Que es un framework ?
El marco de trabajo es un esqueleto para desarrollar o implementar  una  aplicacion, 
siendo esto  un modelo para  la forma de desarrollar software 
"Típicamente, puede incluir soporte de programas, bibliotecas, y un lenguaje interpretado, 
entre otras herramientas, para así ayudar a desarrollar y unir los diferentes componentes de un proyecto." Wikipedia

## ¿Que es Ruby on Rails?

Rails fue creado en 2003 por David Heinemeier Hansson
Es un framework escrito en Ruby, que hace mas agil la  creacion Web,
"Te permite escribir un buen código evitando que te repitas y favoreciendo la convención antes que la configuración." Ruby on Rails
* Utiliza ActiveRecord ---> Para comunicar con la base de datos.
* Utiliza MVC ---->Promueve  la buenas  practicas de desarrollo
* Hay una comunidad activa y fuerte 
* Tiene mas de 10 años de desarrollo
* Muchos startups usan Rails ---> mucha demanda por el desarrollo
* Filosofia DRY (Don't Repeat Yourself)

## ¿Quien utiliza Rails?
Twitter, Scribd, Hulu, Xing, Soundcloud, Basecamp, Github



